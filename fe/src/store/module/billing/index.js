import API from '../../base/'

export default {
  namespaced: true,
  state: {
   billing: {},
  },
  getters: {
    GET_PATIENTS(state) {
      return state.billing;
    }
  },
  mutations: {
    SET_BILLING(state, data) {
      state.billing = data
    },
    REMOVE_BILLING(state, id){
     state.billing.data = state.billing.data.filter(billing => {
      return billing.id !== id;
     });
    },
    PUSH_NEW_BILLING(state, data){
     state.billing.data.push(data);
    }
  },
  actions: {
    async getBillings({commit}, page){
      const res = await API.get(`/admin/billing?page=${page}`).then(res => {
        commit('SET_BILLING', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async searchBilling({commit}, {data, page}){
      const res = await API.post(`/admin/search/billing?page=${page}`, data).then(res => {
        commit('SET_BILLING', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async deleteBilling({commit}, id){
      const res = await API.delete(`/admin/billing/${id}`).then(res => {
        commit('REMOVE_BILLING', id)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async saveBilling({commit}, data){
      const res = await API.post('/admin/billing', data).then(res => {
        commit('PUSH_NEW_BILLING', res.data.billing)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async updateBilling({commit}, data){
      const res = await API.put(`/admin/billing/${data.id}`, data).then(res => {

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
  },
}