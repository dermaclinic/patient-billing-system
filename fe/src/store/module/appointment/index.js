import API from '../../base/'

export default {
  namespaced: true,
  state: {
   appointments: [],
   summary: [],
  },
  getters: {
    GET_APPOINTMENTS(state) {
      return state.appointments;
    }
  },
  mutations: {
    SET_APPOINTMENTS(state, data) {
      state.appointments = data
    },
    SET_SUMMARY(state, data) {
      state.summary = data
    },
    PUSH_NEW_APPOINTMENT(state, data){
     state.appointments.push(data);
    }
  },
  actions: {
    async saveAppointment({commit}, data){
      const res = await API.post('/admin/appointment', data).then(res => {
        commit('PUSH_NEW_APPOINTMENT', res.data.appointment)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getAppointments({commit}){
     const res = await API.get('/admin/appointment').then(res => {
      commit('SET_APPOINTMENTS', res.data)
     }).catch(err => {
      return err.response;
     })

     return res;
    },
    async deleteAppointment({commit}, id){
     const res = await API.delete(`/admin/appointment/${id}`).then(res => {
      return res;
     }).catch(err => {
      return err.response;
     })

     return res;
    },
    async getSummary({commit}){
      const res = await API.get('/admin/summary').then(res => {
        commit('SET_SUMMARY', res.data)
       }).catch(err => {
        return err.response;
       })
  
       return res;
    }
  },
}