<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\BillingInfo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function summary(){
        $mytime = Carbon::now()->format('Y-m-d');

        $appointment = Appointment::whereDate('start', $mytime)->where('user_id', auth('api')->user()->id)->count();
        $billing = BillingInfo::where('user_id', auth('api')->user()->id)->count();
        return response()->json(['appointment' => $appointment, 'billing' => $billing]);
    }

    public function index()
    {
        return response()->json(Appointment::with(['user', 'user.info'])->where('user_id', auth('api')->user()->id)->get());
    }

    public function billing_index()
    {
        return response()->json(BillingInfo::with(['user', 'user.info'])->where('user_id', auth('api')->user()->id)->paginate(10));
    }
}
