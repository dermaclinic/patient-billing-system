<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        return response()->json(User::with(['info'])->paginate(10));
    }

    public function search(){
        $user = User::whereHas('info', function ($query){
            $query->where('first_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('middle_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('last_name', 'like', '%'.request()->get('search').'%');
        })->with('info')->paginate(10);
        return response()->json($user);
    }

    public function delete($id){
        User::destroy($id);
        return response()->json(['msg' => 'Patient deleted successfully!'], 200);
    }

}
