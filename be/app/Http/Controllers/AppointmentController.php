<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return response()->json(Appointment::with(['user', 'user.info'])->get());
    }

    public function store(Request $request){
        $data = [
            'user_id' => $request->user_id,
            'title' => $request->name,
            'content' => $request->content,
            'start' => $request->time[0],
            'end' => $request->time[1],
        ];

        $appointment = Appointment::create($data);
        $added_appointment = Appointment::with(['user', 'user.info'])->where('id', $appointment->id)->first();
        return response()->json(['msg' => 'Appointment added successfully!', 'appointment' => $added_appointment], 200);
    }

    public function destroy(Request $request, $id){
        Appointment::destroy($id);
        return response()->json(['msg' => 'Appointment deleted successfully'], 200);
    }
}
