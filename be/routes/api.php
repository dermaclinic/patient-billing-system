<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\BillingController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserManagementController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function (){
        Route::post('login', [AdminController::class, 'login']);
        Route::post('logout', [AdminController::class, 'logout']);
        Route::put('update/{id}', [AdminController::class, 'update']);
        Route::post('me', [AdminController::class, 'me']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::post('store', [UserController::class, 'store']);
        Route::post('login', [UserController::class, 'login']);
        Route::post('logout', [UserController::class, 'logout']);
        Route::put('update/{id}', [UserController::class, 'update']);
        Route::post('me', [UserController::class, 'me']);
    });
});

Route::group(['middleware' => 'api', 'prefix' => 'admin'], function () {
    Route::get('summary', [AdminController::class, 'summary']);

    Route::get('patients', [PatientController::class, 'index']);
    Route::delete('patients/{id}', [PatientController::class, 'delete']);
    Route::post('search/patients', [PatientController::class, 'search']);
    Route::put('account/update/{id}', [AdminController::class, 'updateUserAccount']);

    Route::get('billing', [BillingController::class, 'index']);
    Route::post('search/billing', [BillingController::class, 'search']);
    Route::post('billing', [BillingController::class, 'store']);
    Route::delete('billing/{id}', [BillingController::class, 'destroy']);
    Route::put('billing/{id}', [BillingController::class, 'update']);

    Route::post('appointment', [AppointmentController::class, 'store']);
    Route::get('appointment', [AppointmentController::class, 'index']);
    Route::delete('appointment/{id}', [AppointmentController::class, 'destroy']);
});

Route::group(['middleware' => 'api', 'prefix' => 'user'], function () {

    Route::get('appointment', [UserManagementController::class, 'index']);
Route::get('billing', [UserManagementController::class, 'billing_index']);
    Route::get('summary', [UserManagementController::class, 'summary']);

});